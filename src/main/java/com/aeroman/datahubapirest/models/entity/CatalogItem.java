package com.aeroman.datahubapirest.models.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "CATALOG_ITEM", schema = "ODS")
public class CatalogItem {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(name = "LABEL")
    private String label;
    @Column(name = "VALUE")
    private String value;
    @Column(name = "STATUS")
    private char status;
    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "CREATED_BY")
    private String createdBy;
    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @ManyToOne
    @JoinColumn
    private Catalog catalog;
}
