package com.aeroman.datahubapirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "C_MROH_CRITICAL_PATHS", schema = "ODS")
public class CriticalPaths implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(name = "CRITICAL_PATH")
    private String criticalPath;
    @Column(name = "TAT_IMPACT")
    private String tatImpact;
    @Column(name = "STATUS")
    private char status;
    @Column(name = "MITIGATION_PLAN")
    private String mitigationPlan;
    @Column(name = "RESPONSIBLE")
    private String responsible;
    @Column(name = "WORK_ORDER")
    private String workOrder;
    @Column(name = "AIRCRAFT")
    private String aircraft;
    @Column(name = "STATUS_FINAL")
    private char statusFinal;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "CREATED_USER")
    private String createdBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "UPDATED_USER")
    private String updatedBy;
    @Column(name = "MROH_SOURCE")
    private String mrohSource;
    @Column(name = "COMPANY")
    private String company;
    @Column(name = "CUSTOMER")
    private String customer;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(String workOrder) {
        this.workOrder = workOrder;
    }

    public String getCriticalPath() {
        return criticalPath;
    }

    public void setCriticalPath(String criticalPath) {
        this.criticalPath = criticalPath;
    }

    public String getTatImpact() {
        return tatImpact;
    }

    public void setTatImpact(String tatImpact) {
        this.tatImpact = tatImpact;
    }

    public String getMitigationPlan() {
        return mitigationPlan;
    }

    public void setMitigationPlan(String mitigationPlan) {
        this.mitigationPlan = mitigationPlan;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public char getStatusFinal() {
        return statusFinal;
    }

    public void setStatusFinal(char statusfinal) {
        this.statusFinal = statusfinal;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMrohSource() {
        return mrohSource;
    }

    public void setMrohSource(String mrohSource) {
        this.mrohSource = mrohSource;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @PrePersist
    public void prePersist() {
        createdAt = new Date();
        updatedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updatedAt = new Date();
    }
}
