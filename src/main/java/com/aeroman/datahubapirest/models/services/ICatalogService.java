package com.aeroman.datahubapirest.models.services;

import com.aeroman.datahubapirest.models.resultset.AircraftInfoResultSet;
import com.aeroman.datahubapirest.models.resultset.CompanyResultSet;
import com.aeroman.datahubapirest.models.resultset.CustomerResultSet;
import com.aeroman.datahubapirest.models.resultset.ItemResultSet;

import java.util.List;

public interface ICatalogService {

    List<CompanyResultSet> getCompanies();

    List<CustomerResultSet> getCustomers(String company);

    List<AircraftInfoResultSet> getAircraft(String company, String customer);

    List<ItemResultSet> getItems(String itemId);
}
