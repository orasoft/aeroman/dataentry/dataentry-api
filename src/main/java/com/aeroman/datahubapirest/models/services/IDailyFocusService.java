package com.aeroman.datahubapirest.models.services;

import com.aeroman.datahubapirest.models.entity.DailyFocus;
import java.util.List;
import java.util.UUID;

public interface IDailyFocusService {

    public List<DailyFocus> findAll();

    public DailyFocus findById(UUID id);

    public DailyFocus save(DailyFocus dailyFocus);

    public void delete(UUID id);
}
