package com.aeroman.datahubapirest.models.resultset;

import java.io.Serializable;

public class AircraftInfoResultSet implements Serializable {
    private String aircraft;
    private Integer woNumber;
    private Integer woCorr;
    private Integer woItem;

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public Integer getWoNumber() {
        return woNumber;
    }

    public void setWoNumber(Integer woNumber) {
        this.woNumber = woNumber;
    }

    public Integer getWoCorr() {
        return woCorr;
    }

    public void setWoCorr(Integer woCorr) {
        this.woCorr = woCorr;
    }

    public Integer getWoItem() {
        return woItem;
    }

    public void setWoItem(Integer woItem) {
        this.woItem = woItem;
    }
}
