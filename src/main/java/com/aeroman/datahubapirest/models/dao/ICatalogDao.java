package com.aeroman.datahubapirest.models.dao;

import com.aeroman.datahubapirest.models.entity.Catalog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ICatalogDao extends CrudRepository<Catalog, UUID> {

    @Query(value = "Select Distinct nvl(OpCo, 'ND') as OpCo from ODS.C_MROH_AIRCRAFT_CHECK", nativeQuery = true)
    List<Object[]> getCompanies();

    @Query(value = "Select Distinct nvl(Customer, 'ND') as Customer from ODS.C_MROH_AIRCRAFT_CHECK Where OpCo=?1", nativeQuery = true)
    List<Object[]> getCustomers(String company);

    @Query(value = "Select distinct nvl(AIRCRAFT, 'ND') as AIRCRAFT , nvl(WO_NUMBER, 0) as WO_NUMBER, nvl(WO_CORR, 0) as WO_CORR, nvl(WO_ITEM, 0) as WO_ITEM from ODS.C_MROH_AIRCRAFT_CHECK Where OpCo=?1 and customer=?2", nativeQuery = true)
    List<Object[]> getAircraft(String company, String customer);

    @Query(value = "SELECT CI.LABEL, CI.VALUE FROM ODS.CATALOG C INNER JOIN ODS.CATALOG_ITEM CI on C.ID = CI.CATALOG_ID WHERE C.STATUS = 'A' AND CI.STATUS = 'A' AND C.NAME = ?1 ORDER BY CI.LABEL", nativeQuery = true)
    List<Object[]> getItems(String itemId);
}
