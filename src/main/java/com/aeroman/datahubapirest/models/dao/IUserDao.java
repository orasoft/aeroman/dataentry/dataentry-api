package com.aeroman.datahubapirest.models.dao;

import com.aeroman.datahubapirest.models.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface IUserDao extends CrudRepository<User, UUID> {

    public User findByUsername(String username);


}
