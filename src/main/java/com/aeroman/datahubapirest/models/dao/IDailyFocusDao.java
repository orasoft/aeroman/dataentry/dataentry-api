package com.aeroman.datahubapirest.models.dao;

import com.aeroman.datahubapirest.models.entity.DailyFocus;
import org.springframework.data.repository.CrudRepository;
import java.util.UUID;

public interface IDailyFocusDao extends CrudRepository<DailyFocus, UUID>{
}
