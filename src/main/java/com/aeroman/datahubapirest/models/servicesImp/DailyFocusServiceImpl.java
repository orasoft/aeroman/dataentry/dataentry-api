package com.aeroman.datahubapirest.models.servicesImp;

import com.aeroman.datahubapirest.models.dao.IDailyFocusDao;
import com.aeroman.datahubapirest.models.entity.DailyFocus;
import com.aeroman.datahubapirest.models.services.IDailyFocusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class DailyFocusServiceImpl implements IDailyFocusService {

    @Autowired
    private IDailyFocusDao dailyFocusDao;

    @Override
    @Transactional(readOnly = true)
    public List<DailyFocus> findAll() {
        return (List<DailyFocus>) dailyFocusDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public DailyFocus findById(UUID id) {
        return dailyFocusDao.findById(id).orElse(null);
    }

    @Override
    public DailyFocus save(DailyFocus dailyFocus) {
        return dailyFocusDao.save(dailyFocus);
    }

    @Override
    public void delete(UUID id) {
        dailyFocusDao.deleteById(id);
    }
}
