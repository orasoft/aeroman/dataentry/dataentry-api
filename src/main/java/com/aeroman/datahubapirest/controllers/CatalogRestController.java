package com.aeroman.datahubapirest.controllers;


import com.aeroman.datahubapirest.message.response.ResponseCriticalPath;
import com.aeroman.datahubapirest.models.resultset.AircraftInfoResultSet;
import com.aeroman.datahubapirest.models.resultset.CompanyResultSet;
import com.aeroman.datahubapirest.models.resultset.CustomerResultSet;
import com.aeroman.datahubapirest.models.resultset.ItemResultSet;
import com.aeroman.datahubapirest.models.services.ICatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/dataentry")
public class CatalogRestController {

    @Autowired
    private ICatalogService catalogService;


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/catalog/items/{id}")
    public List<ItemResultSet> getItems(@PathVariable String id) {
        ResponseCriticalPath response = new ResponseCriticalPath();
        List<ItemResultSet> data = catalogService.getItems(id);
        return data;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/catalog/company")
    public List<CompanyResultSet> getCompanies() {
        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CompanyResultSet> data = catalogService.getCompanies();
        return data;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/catalog/customer/{company}")
    public List<CustomerResultSet> getCustomers(@PathVariable String company) {
        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CustomerResultSet> data = catalogService.getCustomers(company);
        return data;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/catalog/aircraft/{company}/{customer}")
    public List<AircraftInfoResultSet> getAircraft(@PathVariable String company, @PathVariable String customer) {
        List<AircraftInfoResultSet> data = catalogService.getAircraft(company, customer);
        return data;
    }
}
