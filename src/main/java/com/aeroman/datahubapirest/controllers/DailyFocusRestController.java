package com.aeroman.datahubapirest.controllers;

import com.aeroman.datahubapirest.message.response.ResponseDailyFocus;
import com.aeroman.datahubapirest.models.entity.DailyFocus;
import com.aeroman.datahubapirest.models.services.IDailyFocusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/dataentry")
public class DailyFocusRestController {

    @Autowired
    private IDailyFocusService dailyFocusService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/daily_focus")
    public ResponseDailyFocus index() {
        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = dailyFocusService.findAll();
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/daily_focus/{id}")
    public ResponseDailyFocus show(@PathVariable UUID id) {
        DailyFocus record = dailyFocusService.findById(id);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        data.add(record);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/daily_focus")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseDailyFocus create(@RequestBody DailyFocus dailyFocus) {
        dailyFocus.setCreatedBy("system");
        dailyFocus.setUpdatedBy("system");

        DailyFocus recordCreated = dailyFocusService.save(dailyFocus);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        data.add(recordCreated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/daily_focus/{id}")
    public ResponseDailyFocus update(@RequestBody DailyFocus dailyFocus, @PathVariable UUID id) {
        DailyFocus dailyFocusActual = dailyFocusService.findById(id);

        dailyFocusActual.setAircraft(dailyFocus.getAircraft());
        dailyFocusActual.setWorkOrder(dailyFocus.getWorkOrder());
        dailyFocusActual.setTask(dailyFocus.getTask());
        dailyFocusActual.setComplete(dailyFocus.getComplete());
        dailyFocusActual.setReason(dailyFocus.getReason());
        dailyFocusActual.setDateId(dailyFocus.getDateId());

        DailyFocus recordUpdated = dailyFocusService.save(dailyFocusActual);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        data.add(recordUpdated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/daily_focus/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseDailyFocus delete(@PathVariable UUID id) {
        dailyFocusService.delete(id);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        response.setData(data);
        return response;
    }
}
