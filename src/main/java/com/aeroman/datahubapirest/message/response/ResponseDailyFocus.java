package com.aeroman.datahubapirest.message.response;

import com.aeroman.datahubapirest.message.response.interfaces.IResponseDailyFocus;
import com.aeroman.datahubapirest.models.entity.DailyFocus;

public class ResponseDailyFocus implements IResponseDailyFocus {

    public Header header;
    public Iterable<DailyFocus> data;

    public ResponseDailyFocus() {
        this.header = new Header();
    }

    @Override
    public void setData(Iterable<DailyFocus> data) {
        this.data = data;
    }

    @Override
    public void setCode(Integer code) {
        this.header.code = code;
    }

    @Override
    public void setDescription(String description) {
        this.header.description = description;
    }
}
