package com.aeroman.datahubapirest.message.response;

import com.aeroman.datahubapirest.message.response.interfaces.IResponseCritialPath;
import com.aeroman.datahubapirest.models.entity.CriticalPaths;

public class ResponseCriticalPath implements IResponseCritialPath {

    public Header header;
    public Iterable<CriticalPaths> data;

    public ResponseCriticalPath() {
        this.header = new Header();
    }

    @Override
    public void setData(Iterable<CriticalPaths> data) {
        this.data = data;
    }

    @Override
    public void setCode(Integer code) {
        this.header.code = code;
    }

    @Override
    public void setDescription(String description) {
        this.header.description = description;
    }
}
