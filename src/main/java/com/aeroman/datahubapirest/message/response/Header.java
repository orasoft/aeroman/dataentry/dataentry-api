package com.aeroman.datahubapirest.message.response;

import java.io.Serializable;


public class Header implements Serializable {
    public Integer code;
    public String description;
    Header() {
        this.code = 200;
        this.description = "Operacion Exitosa";
    }
}
