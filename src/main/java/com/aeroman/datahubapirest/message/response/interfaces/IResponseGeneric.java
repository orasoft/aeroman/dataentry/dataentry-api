package com.aeroman.datahubapirest.message.response.interfaces;

public interface IResponseGeneric<T> {
    void setData(Iterable<T> data);
    void setCode(Integer code);
    void setDescription(String description);
}
