package com.aeroman.datahubapirest.message.response.interfaces;
import com.aeroman.datahubapirest.models.entity.DailyFocus;

public interface IResponseDailyFocus extends IResponseGeneric<DailyFocus> {
}
