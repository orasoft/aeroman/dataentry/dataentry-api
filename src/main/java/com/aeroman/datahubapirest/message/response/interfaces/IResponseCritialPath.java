package com.aeroman.datahubapirest.message.response.interfaces;

import com.aeroman.datahubapirest.models.entity.CriticalPaths;

public interface IResponseCritialPath extends IResponseGeneric<CriticalPaths> {
}
